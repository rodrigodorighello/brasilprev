# Loja Virtual BRASILPREV

Desafio de construir uma API REST para simular uma loja virtual. Esta loja deve ter um cadastro de seus
clientes, produtos e pedidos.

## Informações

O projeto foi publicado no heroku. A documentação foi feita sobre o swagger. O acesso é através do link 
[http://bp-lojavirtual-api.herokuapp.com/swagger-ui.html](http://bp-lojavirtual-api.herokuapp.com/swagger-ui.html)

- Para cadastrar um usuário faça um POST para /api/auth/signup
- Para conseguir o acess token faça um POST para /api/auth/signin
- Com o token de acesso envie no cabeçalho da requisição o Authorization com o padrão 'Bearer <<acess_token>>'

## Execução Local

É possível importar o projeto em sua IDE e executar a classe SpringBootRun.java.

Caso prefira executar via linha de comando, seguem as instruções abaixo:

Abra o terminal na raiz do projeto e instale o projeto maven

```bash
mvn install
```

Rode o comando para executar o spring boot e subir a aplicação

```bash
mvn spring-boot:run
```

Ao terminar de subir a aplicação, o link para o acesso à documentação será: 
[http://localhost:8080/swagger-ui.html](http://localhost:8080/swagger-ui.html)

## Extras
É possível utilizar o banco Postgres descomentando a linha 2 do application.properties e definindo a conexão com o banco
