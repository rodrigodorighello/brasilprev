package br.com.brasilprev.lojavirtual.domain.repository.jpa.specification;

import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;

import org.springframework.data.jpa.domain.Specification;

import br.com.brasilprev.lojavirtual.domain.model.Order;

@SuppressWarnings("serial")
public class OrderSpecification {
	
	public static Specification<Order> whereByQuantity(Integer quantity) {
		return new Specification<Order>() {

			@Override
			public Predicate toPredicate(Root<Order> root, CriteriaQuery<?> query, CriteriaBuilder cb) {
				return cb.equal(root.<Integer> get("quantity"), quantity);
			}
		};
	}
}
