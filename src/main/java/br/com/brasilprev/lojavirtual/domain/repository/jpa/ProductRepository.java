package br.com.brasilprev.lojavirtual.domain.repository.jpa;

import java.util.List;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.data.jpa.repository.JpaRepository;

import br.com.brasilprev.lojavirtual.domain.model.Product;

public interface ProductRepository extends JpaRepository<Product, Long> {
	
	public List<Product> findAll(Specification<Product> where);
	
	public Page<Product> findAll(Specification<Product> where, Pageable pageable);
}
