package br.com.brasilprev.lojavirtual.domain.service;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.stereotype.Service;

import br.com.brasilprev.lojavirtual.domain.model.Customer;
import br.com.brasilprev.lojavirtual.domain.repository.jpa.CustomerRepository;
import br.com.brasilprev.lojavirtual.domain.repository.jpa.specification.CustomerSpecification;
import br.com.brasilprev.lojavirtual.domain.util.SpecificationUtil;

@Service
public class CustomerService {
	
	@Autowired
	private CustomerRepository customerRepository;
	
	public Page<Customer> findAll(Pageable pageable){
		return customerRepository.findAll(pageable);
	}
	
	public List<Customer> findAll(Customer customerFilter){
		Specification<Customer> specification = geraSpecification(customerFilter);
		
		return customerRepository.findAll(specification);
	}
	
	public Page<Customer> findAll(Customer customerFilter, Pageable pageable){
		Specification<Customer> specification = geraSpecification(customerFilter);
		
		return customerRepository.findAll(specification, pageable);
	}
	
	@SuppressWarnings("unchecked")
	private Specification<Customer> geraSpecification(Customer customerFilter) {
		
		Specification<Customer> specification = null;
		
		if(customerFilter.getName() != null && !customerFilter.getName().trim().isEmpty()) {
			specification = SpecificationUtil.addClausulaAnd(specification, CustomerSpecification.whereByName(customerFilter.getName()));
		}
		
		return specification;
	}
	
	public Optional<Customer> findById(Long id) {
		return customerRepository.findById(id);
	}
	
	public void deleteById(Long id) {
		customerRepository.deleteById(id);
	}
	
	public Customer save(Customer entity) {
		return customerRepository.save(entity);
	}

}
