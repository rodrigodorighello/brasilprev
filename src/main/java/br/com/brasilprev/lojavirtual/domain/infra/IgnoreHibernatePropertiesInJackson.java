package br.com.brasilprev.lojavirtual.domain.infra;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

@JsonIgnoreProperties({ "hibernateLazyInitializer", "handler" })
public abstract class IgnoreHibernatePropertiesInJackson {
	
}