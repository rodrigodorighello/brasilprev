package br.com.brasilprev.lojavirtual.domain.repository.jpa;

import java.util.Optional;

import org.springframework.data.jpa.repository.JpaRepository;

import br.com.brasilprev.lojavirtual.domain.model.CustomerCredential;

public interface CustomerCredentialRepository extends JpaRepository<CustomerCredential, Long> {
	
	public Optional<CustomerCredential> findByUsername(String name);

	public Boolean existsByUsername(String name);
}
