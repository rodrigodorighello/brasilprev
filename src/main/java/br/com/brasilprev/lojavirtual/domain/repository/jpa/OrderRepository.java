package br.com.brasilprev.lojavirtual.domain.repository.jpa;

import java.util.List;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.data.jpa.repository.JpaRepository;

import br.com.brasilprev.lojavirtual.domain.model.Order;

public interface OrderRepository extends JpaRepository<Order, Long> {
	
	public List<Order> findAll(Specification<Order> where);
	
	public Page<Order> findAll(Specification<Order> where, Pageable pageable);
}
