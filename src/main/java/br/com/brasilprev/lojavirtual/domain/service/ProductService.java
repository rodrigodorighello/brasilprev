package br.com.brasilprev.lojavirtual.domain.service;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.stereotype.Service;

import br.com.brasilprev.lojavirtual.domain.model.Product;
import br.com.brasilprev.lojavirtual.domain.repository.jpa.ProductRepository;
import br.com.brasilprev.lojavirtual.domain.repository.jpa.specification.ProductSpecification;
import br.com.brasilprev.lojavirtual.domain.util.SpecificationUtil;

@Service
public class ProductService {
	
	@Autowired
	private ProductRepository productRepository;
	
	public Page<Product> findAll(Pageable pageable){
		return productRepository.findAll(pageable);
	}
	
	public List<Product> findAll(Product productFilter){
		Specification<Product> specification = geraSpecification(productFilter);
		
		return productRepository.findAll(specification);
	}
	
	public Page<Product> findAll(Product productFilter, Pageable pageable){
		Specification<Product> specification = geraSpecification(productFilter);
		
		return productRepository.findAll(specification, pageable);
	}
	
	@SuppressWarnings("unchecked")
	private Specification<Product> geraSpecification(Product productFilter) {
		
		Specification<Product> specification = null;
		
		if(productFilter.getName() != null && !productFilter.getName().trim().isEmpty()) {
			specification = SpecificationUtil.addClausulaAnd(specification, ProductSpecification.whereByName(productFilter.getName()));
		}
		
		return specification;
	}
	
	public Optional<Product> findById(Long id) {
		return productRepository.findById(id);
	}
	
	public void deleteById(Long id) {
		productRepository.deleteById(id);
	}
	
	public Product save(Product entity) {
		return productRepository.save(entity);
	}

}
