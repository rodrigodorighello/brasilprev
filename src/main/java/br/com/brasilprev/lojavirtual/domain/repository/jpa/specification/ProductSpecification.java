package br.com.brasilprev.lojavirtual.domain.repository.jpa.specification;

import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;

import org.springframework.data.jpa.domain.Specification;

import br.com.brasilprev.lojavirtual.domain.model.Product;

@SuppressWarnings("serial")
public class ProductSpecification {
	
	public static Specification<Product> whereByName(String name) {
		return new Specification<Product>() {

			@Override
			public Predicate toPredicate(Root<Product> root, CriteriaQuery<?> query, CriteriaBuilder cb) {
				return cb.like(cb.lower(root.<String> get("name")), "%" + name.trim().toLowerCase() + "%");
			}
		};
	}
}
