package br.com.brasilprev.lojavirtual.domain.repository.jpa;

import java.util.List;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.data.jpa.repository.JpaRepository;

import br.com.brasilprev.lojavirtual.domain.model.Customer;

public interface CustomerRepository extends JpaRepository<Customer, Long> {
	
	public List<Customer> findAll(Specification<Customer> where);
	
	public Page<Customer> findAll(Specification<Customer> where, Pageable pageable);
	
}
