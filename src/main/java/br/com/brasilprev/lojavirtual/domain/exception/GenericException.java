package br.com.brasilprev.lojavirtual.domain.exception;

public class GenericException extends Exception {

	private static final long serialVersionUID = -3727108557944187346L;

	public GenericException(String message) {
		super(message);
	}
	
	public GenericException(String message, Throwable throwable) {
		super(message, throwable);
	}
	
	public String getExceptionDetails() {
		StringBuilder detailedExceptionMessage = new StringBuilder();
		
		detailedExceptionMessage.append(this.getMessage() != null ? this.getMessage() : "Exception message not present.");

		return detailedExceptionMessage.toString();
	}
}
