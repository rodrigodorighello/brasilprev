package br.com.brasilprev.lojavirtual.domain.repository.jpa.specification;

import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;

import org.springframework.data.jpa.domain.Specification;

import br.com.brasilprev.lojavirtual.domain.model.Customer;

@SuppressWarnings("serial")
public class CustomerSpecification {

	public static Specification<Customer> whereByName(String name) {
		return new Specification<Customer>() {

			@Override
			public Predicate toPredicate(Root<Customer> root, CriteriaQuery<?> query, CriteriaBuilder cb) {
				return cb.like(cb.lower(root.<String> get("name")), "%" + name.trim().toLowerCase() + "%");
			}
		};
	}
}
