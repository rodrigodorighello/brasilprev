package br.com.brasilprev.lojavirtual.domain.service;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.stereotype.Service;

import br.com.brasilprev.lojavirtual.domain.model.Order;
import br.com.brasilprev.lojavirtual.domain.repository.jpa.OrderRepository;
import br.com.brasilprev.lojavirtual.domain.repository.jpa.specification.OrderSpecification;
import br.com.brasilprev.lojavirtual.domain.util.SpecificationUtil;

@Service
public class OrderService {
	
	@Autowired
	private OrderRepository orderRepository;
	
	public Page<Order> findAll(Pageable pageable){
		return orderRepository.findAll(pageable);
	}
	
	public List<Order> findAll(Order orderFilter){
		Specification<Order> specification = geraSpecification(orderFilter);
		
		return orderRepository.findAll(specification);
	}
	
	public Page<Order> findAll(Order orderFilter, Pageable pageable){
		Specification<Order> specification = geraSpecification(orderFilter);
		
		return orderRepository.findAll(specification, pageable);
	}
	
	@SuppressWarnings("unchecked")
	private Specification<Order> geraSpecification(Order orderFilter) {
		
		Specification<Order> specification = null;
		
		if(orderFilter.getQuantity() != null) {
			specification = SpecificationUtil.addClausulaAnd(specification, OrderSpecification.whereByQuantity(orderFilter.getQuantity()));
		}
		
		return specification;
	}
	
	public Optional<Order> findById(Long id) {
		return orderRepository.findById(id);
	}
	
	public void deleteById(Long id) {
		orderRepository.deleteById(id);
	}
	
	public Order save(Order entity) {
		return orderRepository.save(entity);
	}

}
