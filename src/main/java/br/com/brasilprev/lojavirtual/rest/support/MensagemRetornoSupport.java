package br.com.brasilprev.lojavirtual.rest.support;

import java.util.HashMap;
import java.util.LinkedList;
import java.util.Map;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;

import br.com.brasilprev.lojavirtual.rest.payload.response.MensagemRetorno;
import br.com.brasilprev.lojavirtual.rest.payload.response.MensagemRetornoCategoria;

public class MensagemRetornoSupport {

	public static ResponseEntity<?> createResponseEntity(MensagemRetornoCategoria categoria, HttpStatus httpStatus){
		return createMensagemRetorno(categoria, httpStatus.getReasonPhrase(), httpStatus, null);
	}
	
	public static ResponseEntity<?> createResponseEntity(MensagemRetornoCategoria categoria, HttpStatus httpStatus, String detalhe){
		Map<String, LinkedList<String>> detalhes = new HashMap<>();
		LinkedList<String> list = new LinkedList<>();
		list.add(detalhe);
		detalhes.put("", list);
		return createMensagemRetorno(categoria, httpStatus.getReasonPhrase(), httpStatus, detalhes);
	}
	
	public static ResponseEntity<?> createResponseEntity(MensagemRetornoCategoria categoria,  HttpStatus httpStatus, Map<String, LinkedList<String>> detalhes){
		return createMensagemRetorno(categoria, httpStatus.getReasonPhrase(), httpStatus, detalhes);
	}
	
	public static ResponseEntity<?> createMensagemRetorno(MensagemRetornoCategoria categoria, String destricao, HttpStatus httpStatus, Map<String, LinkedList<String>> detalhes){
		if(detalhes == null) {
			detalhes = new HashMap<>();
		}
		
		return ResponseEntitySupport.buildResponse(
				builder()
					.categoria(categoria)
					.codigo(httpStatus.value())
					.descricao(destricao)
					.detalhes(detalhes)
					.buildMensagem(),
					httpStatus
				); 
	}
	
	private MensagemRetornoCategoria categoria;
	private int codigo;
	private String descricao; 
	private Map<String, LinkedList<String>> detalhes;
	
	private MensagemRetornoSupport() {
	}
	
	public static MensagemRetornoSupport builder(){
		return new MensagemRetornoSupport();
	}
	
	public MensagemRetornoSupport categoria(MensagemRetornoCategoria categoria) {
		this.categoria = categoria;
		return this;
	}

	public MensagemRetornoSupport codigo(int codigo) {
		this.codigo = codigo;
		return this;
	}

	public MensagemRetornoSupport descricao(String descricao) {
		this.descricao = descricao;
		return this;
	}

	public MensagemRetornoSupport detalhes(Map<String, LinkedList<String>> detalhes) {
		this.detalhes = detalhes;
		return this;
	}
	
	public MensagemRetorno buildMensagem(){
		return buildMensagem(this.categoria, this.descricao, this.codigo, this.detalhes);
	}
	
	public static MensagemRetorno buildMensagem(int codigo, Map<String, LinkedList<String>> detalhes){
		return buildMensagem(null, null, codigo, detalhes);
	}
	
	public static MensagemRetorno buildMensagem(String descricao, int codigo, Map<String, LinkedList<String>> detalhes){
		return buildMensagem(null, descricao, codigo, detalhes);
	}
	
	public static MensagemRetorno buildMensagem(MensagemRetornoCategoria categoria, int codigo, Map<String, LinkedList<String>> detalhes){
		return buildMensagem(categoria, null, codigo, detalhes);
	}
	
	public static MensagemRetorno buildMensagem(MensagemRetornoCategoria categoria, String descricao, int codigo, Map<String, LinkedList<String>> detalhes){
		return new MensagemRetorno(categoria, descricao, codigo, detalhes);
	}
}
