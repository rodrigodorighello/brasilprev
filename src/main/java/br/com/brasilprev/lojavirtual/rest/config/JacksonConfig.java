package br.com.brasilprev.lojavirtual.rest.config;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.SerializationFeature;

import br.com.brasilprev.lojavirtual.domain.infra.IgnoreHibernatePropertiesInJackson;

@Configuration
public class JacksonConfig {
    
	@Bean
	public ObjectMapper objectMapper(){
		
		ObjectMapper mapper = new ObjectMapper();

		// does not fails if empty
		mapper.configure(SerializationFeature.FAIL_ON_EMPTY_BEANS, false);
		// does not fails if unknown properties
		mapper.configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES, false);
		
		// serialize on UperCamelCase strategy
		//mapper.setPropertyNamingStrategy(PropertyNamingStrategy.UPPER_CAMEL_CASE);

		//mapper.enable(SerializationFeature.INDENT_OUTPUT); //pretty print

		// serialize only non null and non empty
		mapper.setSerializationInclusion(JsonInclude.Include.NON_NULL);
		mapper.setSerializationInclusion(JsonInclude.Include.NON_EMPTY);

		// avoid serialization of hibernate proxies properties
		mapper.addMixIn(Object.class, IgnoreHibernatePropertiesInJackson.class);
		
		return mapper;

	}
}
