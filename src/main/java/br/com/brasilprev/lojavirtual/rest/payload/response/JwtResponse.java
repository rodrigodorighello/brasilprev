package br.com.brasilprev.lojavirtual.rest.payload.response;

import java.util.List;
import java.util.stream.Collectors;

import br.com.brasilprev.lojavirtual.rest.security.user.UserDetailsImpl;

public class JwtResponse {
	
	private String accessToken;
	
	private String username;
	
	private List<String> roles;

	public JwtResponse(String accessToken, UserDetailsImpl userDetails) {
		this.accessToken = accessToken;
		this.username = userDetails.getUsername();
		
		List<String> roles = userDetails.getAuthorities().stream()
				.map(item -> item.getAuthority())
				.collect(Collectors.toList());
		this.roles = roles;
	}

	public String getAccessToken() {
		return accessToken;
	}

	public void setAccessToken(String accessToken) {
		this.accessToken = accessToken;
	}

	public String getUsername() {
		return username;
	}

	public void setUsername(String username) {
		this.username = username;
	}

	public List<String> getRoles() {
		return roles;
	}
}
