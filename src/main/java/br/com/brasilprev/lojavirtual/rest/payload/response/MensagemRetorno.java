package br.com.brasilprev.lojavirtual.rest.payload.response;

import java.util.HashMap;
import java.util.LinkedList;
import java.util.Map;

public class MensagemRetorno {

	private MensagemRetornoCategoria categoria;
	private int codigo;
	private String descricao; 
	private Map<String, LinkedList<String>> detalhes;

	public MensagemRetorno() {
		// TODO Auto-generated constructor stub
	}
	
	public MensagemRetorno(int code, Map<String, LinkedList<String>> detalhes) {
		this(null, null, code, detalhes);
	}
	
	public MensagemRetorno(String descricao, int code, Map<String, LinkedList<String>> detalhes) {
		this(null, descricao, code, detalhes);
	}
	
	public MensagemRetorno(MensagemRetornoCategoria categoria, int code, Map<String, LinkedList<String>> detalhes) {
		this(categoria, null, code, detalhes);
	}
	
	public MensagemRetorno(MensagemRetornoCategoria categoria, String descricao, int codigo, Map<String, LinkedList<String>> detalhes) {
		this.categoria = categoria;
		this.descricao = descricao;
		this.codigo = codigo;
		
		if(detalhes == null) 
			detalhes = new HashMap<>();
		
		this.detalhes = detalhes;
	}

	public MensagemRetornoCategoria getCategoria() {
		return categoria;
	}

	public void setCategoria(MensagemRetornoCategoria categoria) {
		this.categoria = categoria;
	}

	public int getCodigo() {
		return codigo;
	}

	public void setCodigo(int codigo) {
		this.codigo = codigo;
	}

	public String getDescricao() {
		return descricao;
	}

	public void setDescricao(String descricao) {
		this.descricao = descricao;
	}
	
	public Map<String, LinkedList<String>> getDetalhes() {
		return detalhes;
	}

	public void setDetalhes(Map<String, LinkedList<String>> detalhes) {
		this.detalhes = detalhes;
	}
	
}
