package br.com.brasilprev.lojavirtual.rest.support;

import java.io.IOException;
import java.io.InputStream;
import java.security.KeyFactory;
import java.security.NoSuchAlgorithmException;
import java.security.interfaces.RSAPrivateKey;
import java.security.interfaces.RSAPublicKey;
import java.security.spec.InvalidKeySpecException;
import java.security.spec.PKCS8EncodedKeySpec;
import java.security.spec.X509EncodedKeySpec;

import org.apache.commons.io.IOUtils;
import org.springframework.core.io.ClassPathResource;
import org.springframework.core.io.Resource;

import br.com.brasilprev.lojavirtual.domain.exception.GenericException;

public class KeyLoaderSupport {
	
	private static final String ALGORITHM = "RSA";
	
	private KeyLoaderSupport() {
	}
	
	public static RSAPrivateKey loadPrivateKey(String privatePath) throws GenericException {
		try {
			
			byte[] privateBytes = getKey(privatePath);
			PKCS8EncodedKeySpec pkcsKey = new PKCS8EncodedKeySpec(privateBytes);
			KeyFactory keyFactory = KeyFactory.getInstance(ALGORITHM);
			return (RSAPrivateKey) keyFactory.generatePrivate(pkcsKey);
		} catch (IOException | InvalidKeySpecException | NoSuchAlgorithmException e) {
			throw new GenericException("Não foi possível extrair a chave privada", e);
		}

	}

	public static RSAPublicKey loadPublicKey(String publicPath) throws GenericException {
		try {
			byte[] publicBytes = getKey(publicPath);
			X509EncodedKeySpec pkcsKey = new X509EncodedKeySpec(publicBytes);
			KeyFactory keyFactory = KeyFactory.getInstance(ALGORITHM);
			return (RSAPublicKey) keyFactory.generatePublic(pkcsKey);
		} catch (IOException | InvalidKeySpecException | NoSuchAlgorithmException e) {
			throw new GenericException("Não foi possível extrair a chave publica", e);
		}
	}

	private static byte[] getKey(String keyPath) throws IOException{
		
		InputStream inputStream = null;
		byte[] certBytes = null;
		
		try {
			Resource resource = new ClassPathResource(keyPath);
			inputStream = resource.getInputStream();
			certBytes = IOUtils.toByteArray(inputStream);
			
		} finally {
			if (inputStream != null) {
				inputStream.close();
			}
		}
		
		return certBytes;
	}
}