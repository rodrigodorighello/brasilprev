package br.com.brasilprev.lojavirtual.rest.controller.docs;

import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.validation.Valid;
import javax.validation.constraints.Min;

import org.springframework.data.domain.Pageable;
import org.springframework.http.ResponseEntity;

import br.com.brasilprev.lojavirtual.domain.model.Customer;
import br.com.brasilprev.lojavirtual.rest.payload.response.MensagemRetorno;
import br.com.brasilprev.lojavirtual.rest.payload.response.Pagination;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;

@Api(value = "/api/customers", tags={"Customers"}, 
description = "Customers Webservices")
public interface CustomerControllerDocs {
	
	@ApiOperation(value = "Customers List Pagination", 
			notes = "Return Customers List Pagination. Failing returns 'MensagemRetorno'")
	@ApiResponses(value = {
			@ApiResponse(code = 200, message = "Customers found.", response = Pagination.class),
			@ApiResponse(code = 404, message = "Customers not found.", response = MensagemRetorno.class)})
	public ResponseEntity<Pagination<Customer>> findAll(Customer customerFilter, @ApiParam(value = "Pageable", required= false) Pageable pageable, HttpServletRequest request);
	
	@ApiOperation(value = "Find a Customer", 
			notes = "Find a Customer. Failing returns 'MensagemRetorno'")
	@ApiResponses(value = {
			@ApiResponse(code = 200, message = "Customer found.", response = Customer.class),
			@ApiResponse(code = 404, message = "Customer not found.", response = MensagemRetorno.class)})
	public ResponseEntity<Customer> findById(@ApiParam(value="Customer ID", required= true) @Min(1) Long id);
	
	@ApiOperation(value = "Add new Customer", 
			notes = "Return new customer. Failing returns 'MensagemRetorno'")
	@ApiResponses(value = {
			@ApiResponse(code = 201, message = "Customer cadastrado com sucesso.", response = Customer.class),
			@ApiResponse(code = 400, message = "Mandatory parameter not found; Validation of unfulfilled rules.", response = MensagemRetorno.class)})
	public ResponseEntity<Customer> create(@Valid Customer newCustomer);
	
	@ApiOperation(value = "Update a Customer", 
			notes = "Return customer updated. Failing returns 'MensagemRetorno'")
	@ApiResponses(value = {
			@ApiResponse(code = 200, message = "Customer atualizado com sucesso.", response = Customer.class),
			@ApiResponse(code = 400, message = "Mandatory parameter not found; Validation of unfulfilled rules.", response = MensagemRetorno.class)})
	public ResponseEntity<Customer> saveOrUpdate(@Valid Customer Customer, @ApiParam(value="Customer ID", required= true) @Min(1) Long id);
	
	@ApiOperation(value = "Updates specific customer fields", 
			notes = "Return customer updated. Failing returns 'MensagemRetorno'")
	@ApiResponses(value = {
			@ApiResponse(code = 200, message = "Customer atualizado com sucesso.", response = Customer.class),
			@ApiResponse(code = 400, message = "Mandatory parameter not found; Validation of unfulfilled rules.", response = MensagemRetorno.class)})
	public ResponseEntity<Customer> patch(Map<String, String> update, @ApiParam(value="Customer ID", required= true) @Min(1) Long id);
	
	@ApiOperation(value = "Delete a Customer", 
			notes = "No payload is returned on success. Failing returns 'MensagemRetorno'")
	@ApiResponses(value = {
			@ApiResponse(code = 400, message = "Mandatory parameter not found; Validation of unfulfilled rules.", response = MensagemRetorno.class)})
	public void delete(@ApiParam(value="Customer ID", required= true) @Min(1) Long id);
}