package br.com.brasilprev.lojavirtual.rest.support;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;

public class ResponseEntitySupport<T> {

	private final T body;
	private final HttpStatus httpStatus;
	private final ResponseEntity<T> responseEntity;

	private ResponseEntitySupport(T body, HttpStatus httpStatus, ResponseEntity<T> responseEntity) {
		this.body = body;
		this.httpStatus = httpStatus;
		this.responseEntity = responseEntity;
	}

	public static <T> ResponseEntitySupport<T> of(HttpStatus httpStatus) {
		return new ResponseEntitySupport<T>(null, httpStatus, null);
	}
	
	public static <T> ResponseEntitySupport<T> of(T body) {
		return new ResponseEntitySupport<T>(body, null, null);
	}
	
	public static <T> ResponseEntitySupport<T> of(T body, HttpStatus httpStatus) {
		return new ResponseEntitySupport<T>(body, body != null ? httpStatus :  null, null);
	}
	
	public static <T> ResponseEntitySupport<T> of(ResponseEntity<T> responseEntity) {
		return new ResponseEntitySupport<T>(null, null, responseEntity);
	}
	
	public ResponseEntity<?> buildResponse() {
		return buildFromInstanceOr(new ResponseEntity<T>(HttpStatus.NO_CONTENT));
	}

	public static <T> ResponseEntity<T> buildResponse(T body) {
		return new ResponseEntity<T>(body, body != null ? HttpStatus.OK : HttpStatus.NO_CONTENT);
	}

	public static <T> ResponseEntity<T> buildResponse(T body, HttpStatus httpStatus) {
		return new ResponseEntity<T>(body, httpStatus);
	}

	public <B> ResponseEntity<?> orElse(B body) {
		return buildFromInstanceOr(new ResponseEntity<B>(body, body != null ? HttpStatus.OK : HttpStatus.NO_CONTENT));
	}

	public <B> ResponseEntity<?> orElse(B body, HttpStatus httpStatus) {
		return buildFromInstanceOr(new ResponseEntity<B>(body, httpStatus));
	}

	public ResponseEntity<?> orElse(HttpStatus httpStatus) {
		return buildFromInstanceOr(new ResponseEntity<>(httpStatus));
	}

	public <B> ResponseEntity<?> orElse(ResponseEntity<B> responseEntity) {
		return buildFromInstanceOr(responseEntity);
	}

	private ResponseEntity<?> buildFromInstanceOr(ResponseEntity<?> responseEntity){
		
		// if there is some 'responseEntity', return it
		if (this.responseEntity != null) {
			return this.responseEntity;
		}
		
		ResponseEntity<?> response = null;
		
		if (this.httpStatus != null) {
			// If there is any httpStatus, create a new ResponseEntity with the given body and httpStatus,
			response = new ResponseEntity<T>(this.body, this.httpStatus);
		}else if (this.body != null) {
			// If there is any body, create a new ResponseEntity with the given body,
			response = new ResponseEntity<T>(this.body,  HttpStatus.OK);
		}else{
			// otherwise return the responseEntity received by parameter
			response = responseEntity;
		}
		
		return response;
	}
}