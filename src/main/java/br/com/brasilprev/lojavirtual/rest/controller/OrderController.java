package br.com.brasilprev.lojavirtual.rest.controller;

import java.util.Map;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.web.PageableDefault;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PatchMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import br.com.brasilprev.lojavirtual.domain.exception.NotFoundException;
import br.com.brasilprev.lojavirtual.domain.exception.OperationException;
import br.com.brasilprev.lojavirtual.domain.model.Order;
import br.com.brasilprev.lojavirtual.domain.service.OrderService;
import br.com.brasilprev.lojavirtual.rest.controller.docs.OrderControllerDocs;
import br.com.brasilprev.lojavirtual.rest.payload.response.Pagination;

@RestController
@Validated
@RequestMapping("/api/orders")
public class OrderController implements OrderControllerDocs {

    @Autowired
    private OrderService orderService;
    
    @GetMapping(value={""}, produces = MediaType.APPLICATION_JSON_VALUE)
	@PreAuthorize("hasRole('BASIC')")
	public ResponseEntity<Pagination<Order>> findAll(Order orderFilter, @PageableDefault Pageable pageable, HttpServletRequest request) {
		Page<Order> page = orderService.findAll(orderFilter, pageable);
    	Pagination<Order> pagination = new Pagination<>(page, request);
		return ResponseEntity.ok(
			pagination
		);
	}
    
    @GetMapping(value={"/{id}"}, produces = MediaType.APPLICATION_JSON_VALUE)
	@PreAuthorize("hasRole('BASIC')")
	public ResponseEntity<Order> findById(@PathVariable Long id) {
		return ResponseEntity.ok(
			orderService.findById(id).orElseThrow(() -> new NotFoundException("Order not found"))
		);
	}
    
    @PostMapping(value={"/create"}, produces = MediaType.APPLICATION_JSON_VALUE)
	@PreAuthorize("hasRole('BASIC')")
	public ResponseEntity<Order> create(@RequestBody Order newOrder) {
		Order order = orderService.save(newOrder);
		return ResponseEntity.ok(
				order
		);
	}
    
    @PutMapping("/{id}")
	@PreAuthorize("hasRole('BASIC')")
    public ResponseEntity<Order> saveOrUpdate(@RequestBody Order order, @PathVariable Long id) {

        return ResponseEntity.ok(
    		orderService.findById(id)
            .map(x -> {
                x.setQuantity(order.getQuantity());
                return orderService.save(x);
            })
            .orElseGet(() -> {
            	order.setId(id);
                return orderService.save(order);
            })
		);
    }
    
    @PatchMapping("/{id}")
	@PreAuthorize("hasRole('BASIC')")
    public ResponseEntity<Order> patch(@RequestBody Map<String, String> update, @PathVariable Long id) {

        return ResponseEntity.ok(
    		orderService.findById(id)
            .map(x -> {

                Integer quantity = Integer.valueOf(update.get("quantity"));
                if (quantity != null && quantity >= 0) {
                    x.setQuantity(quantity);

                    // better create a custom method to update a value = :newValue where id = :id
                    return orderService.save(x);
                } else {
                    throw new OperationException("Atualização do(s) campo(s) " + update.keySet().toString() + " não é permitida.");
                }

            })
            .orElseGet(() -> {
                throw new NotFoundException("Order não encontrado");
            })
		);
    }

    @DeleteMapping("/{id}")
	@PreAuthorize("hasRole('BASIC')")
    public void delete(@PathVariable Long id) {
    	orderService.deleteById(id);
    }

}
