package br.com.brasilprev.lojavirtual.rest.payload.request;

import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.Size;
 
public class RegisterRequest {

    @NotEmpty(message = "Username required")
    @Size(min = 3, max = 20)
    private String username;

    @NotEmpty(message = "Password required")
    @Size(min = 6, max = 40)
    private String password;

    @NotEmpty(message = "Name required")
    @Size(min=3, max = 50)
    private String name;

	public String getUsername() {
		return username;
	}

	public void setUsername(String username) {
		this.username = username;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

}
