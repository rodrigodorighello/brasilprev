package br.com.brasilprev.lojavirtual.rest.controller;

import java.util.Map;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.web.PageableDefault;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.util.StringUtils;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PatchMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import br.com.brasilprev.lojavirtual.domain.exception.NotFoundException;
import br.com.brasilprev.lojavirtual.domain.exception.OperationException;
import br.com.brasilprev.lojavirtual.domain.model.Product;
import br.com.brasilprev.lojavirtual.domain.service.ProductService;
import br.com.brasilprev.lojavirtual.rest.controller.docs.ProductControllerDocs;
import br.com.brasilprev.lojavirtual.rest.payload.response.Pagination;

@RestController
@Validated
@RequestMapping("/api/products")
public class ProductController implements ProductControllerDocs {

    @Autowired
    private ProductService productService;
    
    @GetMapping(value={""}, produces = MediaType.APPLICATION_JSON_VALUE)
	@PreAuthorize("hasRole('BASIC')")
	public ResponseEntity<Pagination<Product>> findAll(Product productFilter, @PageableDefault Pageable pageable, HttpServletRequest request) {
		Page<Product> page = productService.findAll(productFilter, pageable);
    	Pagination<Product> pagination = new Pagination<>(page, request);
		return ResponseEntity.ok(
				pagination
		);
	}
    
    @GetMapping(value={"/{id}"}, produces = MediaType.APPLICATION_JSON_VALUE)
	@PreAuthorize("hasRole('BASIC')")
	public ResponseEntity<Product> findById(@PathVariable Long id) {
		return ResponseEntity.ok(
			productService.findById(id).orElseThrow(() -> new NotFoundException("Product not found"))
		);
	}
    
    @PostMapping(value={"/create"}, produces = MediaType.APPLICATION_JSON_VALUE)
	@PreAuthorize("hasRole('BASIC')")
	public ResponseEntity<Product> create(@RequestBody Product newProduct) {
		Product product = productService.save(newProduct);
		return ResponseEntity.ok(
			product
		);
	}
    
    @PutMapping("/{id}")
	@PreAuthorize("hasRole('BASIC')")
    public ResponseEntity<Product> saveOrUpdate(@RequestBody Product product, @PathVariable Long id) {

        return ResponseEntity.ok(
    		productService.findById(id)
            .map(x -> {
                x.setName(product.getName());
                return productService.save(x);
            })
            .orElseGet(() -> {
            	product.setId(id);
                return productService.save(product);
            })
		);
    }
    
    @PatchMapping("/{id}")
	@PreAuthorize("hasRole('BASIC')")
    public ResponseEntity<Product> patch(@RequestBody Map<String, String> update, @PathVariable Long id) {

        return ResponseEntity.ok(
    		productService.findById(id)
            .map(x -> {

                String name = update.get("name");
                if (!StringUtils.isEmpty(name)) {
                    x.setName(name);

                    // better create a custom method to update a value = :newValue where id = :id
                    return productService.save(x);
                } else {
                    throw new OperationException("Atualização do(s) campo(s) " + update.keySet().toString() + " não é permitida.");
                }

            })
            .orElseGet(() -> {
                throw new NotFoundException("Product not found");
            })
		);
    }

    @DeleteMapping("/{id}")
	@PreAuthorize("hasRole('BASIC')")
    public void delete(@PathVariable Long id) {
    	productService.deleteById(id);
    }

}
