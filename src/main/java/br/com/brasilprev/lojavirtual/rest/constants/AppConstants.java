package br.com.brasilprev.lojavirtual.rest.constants;

public class AppConstants {
	
	public static final String SWAGGER_PACKAGE_DOCS = "br.com.brasilprev.lojavirtual.rest.controller";
}
