package br.com.brasilprev.lojavirtual.rest.controller;

import java.util.Date;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import br.com.brasilprev.lojavirtual.domain.exception.OperationException;
import br.com.brasilprev.lojavirtual.domain.model.Customer;
import br.com.brasilprev.lojavirtual.domain.model.CustomerCredential;
import br.com.brasilprev.lojavirtual.domain.repository.jpa.CustomerCredentialRepository;
import br.com.brasilprev.lojavirtual.domain.repository.jpa.CustomerRepository;
import br.com.brasilprev.lojavirtual.rest.controller.docs.AuthControllerDocs;
import br.com.brasilprev.lojavirtual.rest.payload.request.LoginRequest;
import br.com.brasilprev.lojavirtual.rest.payload.request.RegisterRequest;
import br.com.brasilprev.lojavirtual.rest.payload.response.JwtResponse;
import br.com.brasilprev.lojavirtual.rest.security.jwt.JwtUtils;
import br.com.brasilprev.lojavirtual.rest.security.user.UserDetailsImpl;

@CrossOrigin(origins = "*", maxAge = 3600)
@RestController
@RequestMapping("/api/auth")
public class AuthController implements AuthControllerDocs {
	
	@Autowired
	private AuthenticationManager authenticationManager;
	
	@Autowired
	private CustomerRepository customerRepository;
	
	@Autowired
	private CustomerCredentialRepository customerCredentialRepository;

	@Autowired
	private PasswordEncoder passwordEncoder;

	@Autowired
	private JwtUtils jwtUtils;

	@PostMapping("/signin")
	public ResponseEntity<JwtResponse> authenticateUser(@RequestBody LoginRequest loginRequest) {
		
		Authentication authentication = authenticationManager.authenticate(new UsernamePasswordAuthenticationToken(loginRequest.getUsername(), loginRequest.getPassword()));

		SecurityContextHolder.getContext().setAuthentication(authentication);
		
		UserDetailsImpl userDetails = (UserDetailsImpl) authentication.getPrincipal();
		
		String accessToken = jwtUtils.gerarToken(userDetails);

		return ResponseEntity.ok(new JwtResponse(accessToken, userDetails));
	}

	@PostMapping("/signup")
	public ResponseEntity<CustomerCredential> registerUser(@RequestBody RegisterRequest registerRequest) {
		if (customerCredentialRepository.existsByUsername(registerRequest.getUsername())) {
            throw new OperationException("Usuário já existente!");
		}
		
		Customer customer = new Customer();
		customer.setName(registerRequest.getName());
		customerRepository.save(customer);
		
		CustomerCredential customerCredential = new CustomerCredential();
		customerCredential.setUsername(registerRequest.getUsername());
		customerCredential.setPassword(passwordEncoder.encode(registerRequest.getPassword()));
		customerCredential.setDateActivation(new Date());
		customerCredentialRepository.save(customerCredential);

		return ResponseEntity.ok(customerCredential);
	}
}
