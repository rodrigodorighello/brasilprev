package br.com.brasilprev.lojavirtual.rest.config;

import java.security.PrivateKey;
import java.security.PublicKey;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.method.configuration.EnableGlobalMethodSecurity;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.config.http.SessionCreationPolicy;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.security.web.authentication.UsernamePasswordAuthenticationFilter;

import br.com.brasilprev.lojavirtual.domain.exception.GenericException;
import br.com.brasilprev.lojavirtual.rest.security.jwt.AuthEntryPointJwt;
import br.com.brasilprev.lojavirtual.rest.security.jwt.filter.AuthTokenFilter;
import br.com.brasilprev.lojavirtual.rest.security.user.service.UserDetailsServiceImpl;
import br.com.brasilprev.lojavirtual.rest.support.KeyLoaderSupport;

@Configuration
@EnableWebSecurity
@EnableGlobalMethodSecurity(
		// securedEnabled = true,
		// jsr250Enabled = true,
		prePostEnabled = true)
public class WebSecurityConfig extends WebSecurityConfigurerAdapter {
	
	@Autowired
	private UserDetailsServiceImpl userDetailsService;

	@Autowired
	private AuthEntryPointJwt unauthorizedHandler;

	@Value("${jwt.path.privateKey}")
	private String pathPrivateKey;
	
	@Value("${jwt.path.publicKey}")
	private String pathPublicKey;

	@Bean
	public AuthTokenFilter authenticationJwtTokenFilter() {
		return new AuthTokenFilter();
	}

	@Override
	public void configure(AuthenticationManagerBuilder authenticationManagerBuilder) throws Exception {
		authenticationManagerBuilder.userDetailsService(userDetailsService).passwordEncoder(passwordEncoder());
	}

	@Bean
	@Override
	public AuthenticationManager authenticationManagerBean() throws Exception {
		return super.authenticationManagerBean();
	}

	@Bean
	public PasswordEncoder passwordEncoder() {
		return new BCryptPasswordEncoder();
	}

	@Bean
	public PrivateKey privateKey() throws GenericException {
		return KeyLoaderSupport.loadPrivateKey(pathPrivateKey);
	}

	@Bean
	public PublicKey publicKey() throws GenericException {
		return KeyLoaderSupport.loadPublicKey(pathPublicKey);
	}

	@Override
	protected void configure(HttpSecurity http) throws Exception {
		http
			.cors()
		.and()
			.csrf()
				.disable()
				.exceptionHandling().
				authenticationEntryPoint(unauthorizedHandler)
		.and()
			.sessionManagement()
				.sessionCreationPolicy(SessionCreationPolicy.STATELESS)
		.and()
			.authorizeRequests()
				.antMatchers("/api/auth/**").permitAll()
				.antMatchers("/api/test/**").permitAll()
//				.antMatchers("/api/**").permitAll()
				.antMatchers("/", "/auth/login", "/auth/register").permitAll()
				.antMatchers("/assets/**", "/*.js", "/*.woff2", "/*.ttf", "/*.ico").permitAll()//angular
				.antMatchers("/swagger-resources/**","/swagger-ui.html", "/v2/api-docs","/webjars/**").permitAll()//swagger
				.anyRequest().authenticated()
			;

		http.addFilterBefore(authenticationJwtTokenFilter(), UsernamePasswordAuthenticationFilter.class);
	}
}
