package br.com.brasilprev.lojavirtual.rest.security.user.service;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import br.com.brasilprev.lojavirtual.domain.model.CustomerCredential;
import br.com.brasilprev.lojavirtual.domain.repository.jpa.CustomerCredentialRepository;
import br.com.brasilprev.lojavirtual.rest.security.user.UserDetailsImpl;

@Service
public class UserDetailsServiceImpl implements UserDetailsService {
	
	@Autowired
	private CustomerCredentialRepository customerCredentialRepository;

	@Override
	@Transactional
	public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
		
		CustomerCredential customerCredential = customerCredentialRepository.findByUsername(username)
				.orElseThrow(() -> new UsernameNotFoundException("Não foi encontrado usuário com nome: " + username));

		List<String> roles = new ArrayList<>();
		roles.add("ROLE_BASIC");
		
		return UserDetailsImpl.build(customerCredential, roles);
	}

}
