package br.com.brasilprev.lojavirtual.rest.controller.docs;

import javax.validation.Valid;

import org.springframework.http.ResponseEntity;

import br.com.brasilprev.lojavirtual.domain.model.CustomerCredential;
import br.com.brasilprev.lojavirtual.rest.payload.request.LoginRequest;
import br.com.brasilprev.lojavirtual.rest.payload.request.RegisterRequest;
import br.com.brasilprev.lojavirtual.rest.payload.response.JwtResponse;
import br.com.brasilprev.lojavirtual.rest.payload.response.MensagemRetorno;
import br.com.brasilprev.lojavirtual.rest.payload.response.MessageResponse;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;

@Api(value = "/api/auth", tags={"Auth"}, 
description = "Auth Webservices")
public interface AuthControllerDocs {
	
	@ApiOperation(value = "User authentication", 
			notes = "Returns the jwt response. Failing returns 'MensagemRetorno'")
	@ApiResponses(value = {
			@ApiResponse(code = 200, message = "Success auth.", response = JwtResponse.class),
			@ApiResponse(code = 400, message = "Mandatory parameter not found; Validation of unfulfilled rules.", response = MensagemRetorno.class)})
	public ResponseEntity<JwtResponse> authenticateUser(@Valid LoginRequest loginRequest);
	
	@ApiOperation(value = "Register new user", 
			notes = "Return user credential. Failing returns 'MensagemRetorno'")
	@ApiResponses(value = {
			@ApiResponse(code = 200, message = "User successfully registered.", response = MessageResponse.class),
			@ApiResponse(code = 400, message = "Mandatory parameter not found; Validation of unfulfilled rules.", response = MensagemRetorno.class)})
	public ResponseEntity<CustomerCredential> registerUser(@Valid RegisterRequest signUpRequest);
	
}