package br.com.brasilprev.lojavirtual.rest.support;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.PropertySource;
import org.springframework.context.annotation.PropertySources;
import org.springframework.core.env.Environment;

@Configuration
@PropertySources({
	@PropertySource(value="classpath:/application.properties", encoding="UTF-8")
})
public class PropertiesSupport {
	
	@Autowired
	private Environment env;

	public String getProperty(String key){
		return env.getProperty(key);
	}
	
	public <T> T getProperty(String key, Class<T> targetType){
		return env.getProperty(key, targetType);
	}
}
