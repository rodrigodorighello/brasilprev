package br.com.brasilprev.lojavirtual.rest.controller.docs;

import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.validation.Valid;
import javax.validation.constraints.Min;

import org.springframework.data.domain.Pageable;
import org.springframework.http.ResponseEntity;

import br.com.brasilprev.lojavirtual.domain.model.Product;
import br.com.brasilprev.lojavirtual.rest.payload.response.MensagemRetorno;
import br.com.brasilprev.lojavirtual.rest.payload.response.Pagination;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;

@Api(value = "/api/products", tags={"Products"}, 
description = "Products Webservices")
public interface ProductControllerDocs {
	
	@ApiOperation(value = "Products List Pagination", 
			notes = "Return Products List Pagination. Failing returns 'MensagemRetorno'")
	@ApiResponses(value = {
			@ApiResponse(code = 200, message = "Products found.", response = Pagination.class),
			@ApiResponse(code = 404, message = "Products not found.", response = MensagemRetorno.class)})
	public ResponseEntity<Pagination<Product>> findAll(Product productFilter, @ApiParam(value = "Pageable", required= false) Pageable pageable, HttpServletRequest request);
	
	@ApiOperation(value = "Find a Product", 
			notes = "Find a Product. Failing returns 'MensagemRetorno'")
	@ApiResponses(value = {
			@ApiResponse(code = 200, message = "Product found.", response = Product.class),
			@ApiResponse(code = 404, message = "Product not found.", response = MensagemRetorno.class)})
	public ResponseEntity<Product> findById(@ApiParam(value="Product ID", required= true) @Min(1) Long id);
	
	@ApiOperation(value = "Add new Product", 
			notes = "Return new product. Failing returns 'MensagemRetorno'")
	@ApiResponses(value = {
			@ApiResponse(code = 201, message = "Product cadastrado com sucesso.", response = Product.class),
			@ApiResponse(code = 400, message = "Parâmetro obrigatório não encontrado; Validação das regras não cumpridas.", response = MensagemRetorno.class)})
	public ResponseEntity<Product> create(@Valid Product newProduct);
	
	@ApiOperation(value = "Update a Product", 
			notes = "Return product updated. Failing returns 'MensagemRetorno'")
	@ApiResponses(value = {
			@ApiResponse(code = 200, message = "Product atualizado com sucesso.", response = Product.class),
			@ApiResponse(code = 400, message = "Parâmetro obrigatório não encontrado; Validação das regras não cumpridas.", response = MensagemRetorno.class)})
	public ResponseEntity<Product> saveOrUpdate(@Valid Product Product, @ApiParam(value="Product ID", required= true) @Min(1) Long id);
	
	@ApiOperation(value = "Updates specific product fields", 
			notes = "Return product updated. Failing returns 'MensagemRetorno'")
	@ApiResponses(value = {
			@ApiResponse(code = 200, message = "Product atualizado com sucesso.", response = Product.class),
			@ApiResponse(code = 400, message = "Parâmetro obrigatório não encontrado; Validação das regras não cumpridas.", response = MensagemRetorno.class)})
	public ResponseEntity<Product> patch(Map<String, String> update, @ApiParam(value="Product ID", required= true) @Min(1) Long id);
	
	@ApiOperation(value = "Delete a Product", 
			notes = "No payload is returned on success. Failing returns 'MensagemRetorno'")
	@ApiResponses(value = {
			@ApiResponse(code = 400, message = "Parâmetro obrigatório não encontrado; Validação das regras não cumpridas.", response = MensagemRetorno.class)})
	public void delete(@ApiParam(value="Product ID", required= true) @Min(1) Long id);
}