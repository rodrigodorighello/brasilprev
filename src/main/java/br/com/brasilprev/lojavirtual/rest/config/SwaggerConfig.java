package br.com.brasilprev.lojavirtual.rest.config;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.http.HttpStatus;
import org.springframework.security.core.annotation.AuthenticationPrincipal;
import org.springframework.web.bind.annotation.RequestMethod;

import com.google.common.collect.Lists;

import br.com.brasilprev.lojavirtual.rest.constants.AppConstants;
import br.com.brasilprev.lojavirtual.rest.support.PropertiesSupport;
import springfox.documentation.builders.ApiInfoBuilder;
import springfox.documentation.builders.ParameterBuilder;
import springfox.documentation.builders.PathSelectors;
import springfox.documentation.builders.RequestHandlerSelectors;
import springfox.documentation.builders.ResponseMessageBuilder;
import springfox.documentation.schema.ModelRef;
import springfox.documentation.service.ApiInfo;
import springfox.documentation.service.Contact;
import springfox.documentation.service.Parameter;
import springfox.documentation.service.ResponseMessage;
import springfox.documentation.spi.DocumentationType;
import springfox.documentation.spring.web.plugins.Docket;
import springfox.documentation.swagger2.annotations.EnableSwagger2;

@Configuration
@EnableSwagger2
public class SwaggerConfig {
	
	public static final String[] SWAGGER_PATHS = new String[]{ "/swagger-resources/**","/swagger-ui.html", "/v2/api-docs","/webjars/**"};

	@Autowired
	private PropertiesSupport propertiesSupport;
	
	@Bean
	public Docket api(ArrayList<ResponseMessage> responsesGobal, ApiInfo apiInfo) {

		// Global Authorization Header
		List<Parameter> headers = new ArrayList<>();
		headers.add(new ParameterBuilder().modelRef(new ModelRef("")).name("Authorization").description("jwt token")
				.parameterType("header").build());

		return new Docket(DocumentationType.SWAGGER_2).select()
				.apis(RequestHandlerSelectors.basePackage(AppConstants.SWAGGER_PACKAGE_DOCS))
				.paths(PathSelectors.any()).build().apiInfo(apiInfo).useDefaultResponseMessages(false)
				.globalResponseMessage(RequestMethod.GET, responsesGobal)
				.globalResponseMessage(RequestMethod.PUT, responsesGobal)
				.globalResponseMessage(RequestMethod.POST, responsesGobal)
				.globalResponseMessage(RequestMethod.DELETE, responsesGobal)
				.ignoredParameterTypes(AuthenticationPrincipal.class)
				.globalOperationParameters(headers);

	}
    
	@Bean
	protected ArrayList<ResponseMessage> responsesGobal() {

		ArrayList<ResponseMessage> responsesGobal = Lists.newArrayList(
				new ResponseMessageBuilder()
					.code(HttpStatus.UNAUTHORIZED.value())
					.message(HttpStatus.UNAUTHORIZED.getReasonPhrase())
					.build(),
				new ResponseMessageBuilder()
					.code(HttpStatus.FORBIDDEN.value())
					.message(HttpStatus.FORBIDDEN.getReasonPhrase())
					.build(),
				new ResponseMessageBuilder()
					.code(HttpStatus.NOT_FOUND.value())
					.message(HttpStatus.NOT_FOUND.getReasonPhrase())
					.build(),
				new ResponseMessageBuilder()
					.code(HttpStatus.INTERNAL_SERVER_ERROR.value())
					.message(HttpStatus.INTERNAL_SERVER_ERROR.getReasonPhrase())
					.build()
				);

		return responsesGobal;
	}

	@Bean
	protected ApiInfo apiInfo() {
 
        ApiInfo apiInfo = new ApiInfoBuilder()
                .title (
                		propertiesSupport.getProperty("swagger.api.title")
                )
                .description (
                		propertiesSupport.getProperty("swagger.api.description")
                )
                .version(
                		propertiesSupport.getProperty("swagger.api.version")
                ).contact(
                	new Contact(
                		propertiesSupport.getProperty("swagger.api.contact.name"), 
                		propertiesSupport.getProperty("swagger.api.contact.url"), 
                		propertiesSupport.getProperty("swagger.api.contact.email")
                	)
                )
                //.license("Apache License Version 2.0")
                //.licenseUrl("https://www.apache.org/licenses/LICENSE-2.0")
                //.termsOfServiceUrl("/service.html")
                .build();
 
        return apiInfo;
    }
}