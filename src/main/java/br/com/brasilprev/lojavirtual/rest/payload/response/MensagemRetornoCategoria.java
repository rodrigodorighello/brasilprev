package br.com.brasilprev.lojavirtual.rest.payload.response;

public enum MensagemRetornoCategoria {
	ALERTA,
	CONFIRMACAO,
	INFOMACAO,
	ERRO
}