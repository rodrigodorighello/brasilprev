package br.com.brasilprev.lojavirtual.rest.error.controller;

import javax.servlet.http.HttpServletRequest;

import org.springframework.boot.web.servlet.error.ErrorController;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import br.com.brasilprev.lojavirtual.rest.payload.response.MensagemRetornoCategoria;
import br.com.brasilprev.lojavirtual.rest.support.MensagemRetornoSupport;

@Controller
public class SpringBootServletErrorController implements ErrorController {

	@RequestMapping("/error")
	@ResponseBody
	public ResponseEntity<?> handleError(HttpServletRequest request) {
		HttpStatus httpStatus = HttpStatus.valueOf(((Integer) request.getAttribute("javax.servlet.error.status_code")));

		if (request.getAttribute("javax.servlet.error.exception") != null && request.getAttribute("javax.servlet.error.exception") instanceof Exception) {
			String message = ((Exception) request.getAttribute("javax.servlet.error.exception")).getMessage();
			return MensagemRetornoSupport.createResponseEntity(MensagemRetornoCategoria.ERRO, httpStatus, message);
		} else {
			return MensagemRetornoSupport.createResponseEntity(MensagemRetornoCategoria.ERRO, httpStatus);
		}
	}

	@Override
	public String getErrorPath() {
		return "/error";
	}
}
