package br.com.brasilprev.lojavirtual.rest.error.controller;

import java.util.HashMap;
import java.util.LinkedList;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;

import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.http.converter.HttpMessageNotReadableException;
import org.springframework.security.access.AccessDeniedException;
import org.springframework.validation.FieldError;
import org.springframework.web.HttpMediaTypeNotSupportedException;
import org.springframework.web.HttpRequestMethodNotSupportedException;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestControllerAdvice;
import org.springframework.web.context.request.WebRequest;
import org.springframework.web.method.annotation.MethodArgumentTypeMismatchException;
import org.springframework.web.servlet.mvc.method.annotation.ResponseEntityExceptionHandler;

import br.com.brasilprev.lojavirtual.domain.exception.NotFoundException;
import br.com.brasilprev.lojavirtual.domain.exception.OperationException;
import br.com.brasilprev.lojavirtual.rest.payload.response.MensagemRetornoCategoria;
import br.com.brasilprev.lojavirtual.rest.support.MensagemRetornoSupport;

@RestControllerAdvice
public class ErrorHandlerRestController extends ResponseEntityExceptionHandler {

    @ExceptionHandler(Throwable.class)
    @ResponseBody
    ResponseEntity<?> handleControllerException(HttpServletRequest request, Throwable ex) {
    	
        if(ex instanceof NotFoundException) {
    		return MensagemRetornoSupport.createResponseEntity(MensagemRetornoCategoria.ALERTA, HttpStatus.NOT_FOUND, ex.getMessage());
    		
        } else if(ex instanceof OperationException) {
        	return MensagemRetornoSupport.createResponseEntity(MensagemRetornoCategoria.ERRO, HttpStatus.INTERNAL_SERVER_ERROR,ex.getMessage());
        	
        } else if(ex instanceof HttpMessageNotReadableException) {
        	return MensagemRetornoSupport.createResponseEntity(MensagemRetornoCategoria.ERRO, HttpStatus.BAD_REQUEST, "Message Payload not readable.");
        	
        } else if(ex instanceof NumberFormatException) {
        	return MensagemRetornoSupport.createResponseEntity(MensagemRetornoCategoria.ERRO, HttpStatus.BAD_REQUEST, "Invalid input number.");
        	
        } else if(ex instanceof MethodArgumentTypeMismatchException) {
        	return MensagemRetornoSupport.createResponseEntity(MensagemRetornoCategoria.ERRO, HttpStatus.BAD_REQUEST, "Invalid input number.");
        	
        } else if(ex instanceof HttpRequestMethodNotSupportedException) {
        	return MensagemRetornoSupport.createResponseEntity(MensagemRetornoCategoria.ERRO, HttpStatus.METHOD_NOT_ALLOWED, "Método não permitido");
        	
        } else if(ex instanceof HttpMediaTypeNotSupportedException) {
        	return MensagemRetornoSupport.createResponseEntity(MensagemRetornoCategoria.ERRO, HttpStatus.UNSUPPORTED_MEDIA_TYPE, "Tipo de mídia não suportado");
        	
        } else if(ex instanceof AccessDeniedException) {
        	return MensagemRetornoSupport.createResponseEntity(MensagemRetornoCategoria.ERRO, HttpStatus.FORBIDDEN, "Sem permissão para realizar essa operação");
        	
        } else {
        	return MensagemRetornoSupport.createResponseEntity(MensagemRetornoCategoria.ERRO, HttpStatus.INTERNAL_SERVER_ERROR, ex.getMessage());
        }
    }
    
    // error handle for @Valid
    @SuppressWarnings("unchecked")
	@Override
    protected ResponseEntity<Object> handleMethodArgumentNotValid(MethodArgumentNotValidException ex,
                                                                  HttpHeaders headers,
                                                                  HttpStatus status, WebRequest request) {
        
		Map<String, LinkedList<String>> errors = new HashMap<>();
		ex.getBindingResult().getAllErrors().forEach((error) -> {
			String fieldName = ((FieldError) error).getField();
			String errorMessage = error.getDefaultMessage();
			
			if(!errors.containsKey(fieldName)) {
				LinkedList<String> listErrors = new LinkedList<>();
				listErrors.add(errorMessage);
				errors.put(fieldName, listErrors);
			} else {
				errors.get(fieldName).add(errorMessage);
			}
		});
		return (ResponseEntity<Object>) MensagemRetornoSupport.createResponseEntity(MensagemRetornoCategoria.ERRO, status, errors);


    }
    
}