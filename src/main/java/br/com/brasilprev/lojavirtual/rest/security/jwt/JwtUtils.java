package br.com.brasilprev.lojavirtual.rest.security.jwt;

import java.security.PrivateKey;
import java.security.PublicKey;
import java.util.Date;
import java.util.List;
import java.util.stream.Collectors;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

import br.com.brasilprev.lojavirtual.rest.constants.AppConstants;
import br.com.brasilprev.lojavirtual.rest.security.user.UserDetailsImpl;
import io.jsonwebtoken.Claims;
import io.jsonwebtoken.ExpiredJwtException;
import io.jsonwebtoken.Jwts;
import io.jsonwebtoken.MalformedJwtException;
import io.jsonwebtoken.SignatureAlgorithm;
import io.jsonwebtoken.SignatureException;
import io.jsonwebtoken.UnsupportedJwtException;

@Component
public class JwtUtils {
	private static final Logger logger = LoggerFactory.getLogger(JwtUtils.class);

	@Value("${jwt.expirationMs}")
	private int jwtExpirationMs;
	
	@Autowired
	private PrivateKey privateKey;
	
	@Autowired
	private PublicKey publicKey;
	
	public String gerarToken(UserDetailsImpl userDetails) {

		Claims claims = Jwts.claims();
		claims.setSubject(userDetails.getUsername());

		claims.put("app_name", "loja-virtual-brasilprev");
		
		List<String> roles = userDetails.getAuthorities().stream()
				.map(item -> item.getAuthority())
				.collect(Collectors.toList());
		claims.put("authorities", roles);
		
		claims.setIssuer("br.com.brasilprev.lojavirtual");
		claims.setIssuedAt(new Date());
		claims.setExpiration(new Date((new Date()).getTime() + jwtExpirationMs));
		
		return Jwts.builder().setClaims(claims)
				.signWith(SignatureAlgorithm.RS512, privateKey)
				.compact(); 
	}

	public UserDetailsImpl gerarUserDetailsImpl(String token) {
		
		String username = getUserNameFromJwtToken(token);

		Claims claims = Jwts.parser().setSigningKey(publicKey).parseClaimsJws(token).getBody();
		
		@SuppressWarnings("unchecked")
		List<String> roles = (List<String>) claims.get("authorities");

		return new UserDetailsImpl(username, null, roles);
		
	}
	
	public String getUserNameFromJwtToken(String token) {
		return Jwts.parser().setSigningKey(publicKey).parseClaimsJws(token).getBody().getSubject();
	}

	public boolean validateJwtToken(String authToken) {
		try {
			Jwts.parser().setSigningKey(publicKey).parseClaimsJws(authToken);
			return true;
		} catch (SignatureException e) {
			logger.error("Invalid JWT signature: {}", e.getMessage());
		} catch (MalformedJwtException e) {
			logger.error("Invalid JWT token: {}", e.getMessage());
		} catch (ExpiredJwtException e) {
			logger.error("JWT token is expired: {}", e.getMessage());
		} catch (UnsupportedJwtException e) {
			logger.error("JWT token is unsupported: {}", e.getMessage());
		} catch (IllegalArgumentException e) {
			logger.error("JWT claims string is empty: {}", e.getMessage());
		}

		return false;
	}
}
