package br.com.brasilprev.lojavirtual.rest.controller.docs;

import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.validation.Valid;
import javax.validation.constraints.Min;

import org.springframework.data.domain.Pageable;
import org.springframework.http.ResponseEntity;

import br.com.brasilprev.lojavirtual.domain.model.Order;
import br.com.brasilprev.lojavirtual.rest.payload.response.MensagemRetorno;
import br.com.brasilprev.lojavirtual.rest.payload.response.Pagination;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;

@Api(value = "/api/orders", tags={"Orders"}, 
description = "Orders Webservices")
public interface OrderControllerDocs {
	
	@ApiOperation(value = "Orders List Pagination", 
			notes = "Return Orders List Pagination. Failing returns 'MensagemRetorno'")
	@ApiResponses(value = {
			@ApiResponse(code = 200, message = "Orders found.", response = Pagination.class),
			@ApiResponse(code = 404, message = "Orders not found.", response = MensagemRetorno.class)})
	public ResponseEntity<Pagination<Order>> findAll(Order orderFilter, @ApiParam(value = "Pageable", required= false) Pageable pageable, HttpServletRequest request);
	
	@ApiOperation(value = "Find a Order", 
			notes = "Find a Order. Failing returns 'MensagemRetorno'")
	@ApiResponses(value = {
			@ApiResponse(code = 200, message = "Order found.", response = Order.class),
			@ApiResponse(code = 404, message = "Order not found.", response = MensagemRetorno.class)})
	public ResponseEntity<Order> findById(@ApiParam(value="Order ID", required= true) @Min(1) Long id);
	
	@ApiOperation(value = "Add new Order", 
			notes = "Return new order. Failing returns 'MensagemRetorno'")
	@ApiResponses(value = {
			@ApiResponse(code = 201, message = "Order cadastrado com sucesso.", response = Order.class),
			@ApiResponse(code = 400, message = "Mandatory parameter not found; Validation of unfulfilled rules.", response = MensagemRetorno.class)})
	public ResponseEntity<Order> create(@Valid Order newOrder);
	
	@ApiOperation(value = "Update a Order", 
			notes = "Return order updated. Failing returns 'MensagemRetorno'")
	@ApiResponses(value = {
			@ApiResponse(code = 200, message = "Order atualizado com sucesso.", response = Order.class),
			@ApiResponse(code = 400, message = "Mandatory parameter not found; Validation of unfulfilled rules.", response = MensagemRetorno.class)})
	public ResponseEntity<Order> saveOrUpdate(@Valid Order Order, @ApiParam(value="Order ID", required= true) @Min(1) Long id);
	
	@ApiOperation(value = "Updates specific order fields", 
			notes = "Return order updated. Failing returns 'MensagemRetorno'")
	@ApiResponses(value = {
			@ApiResponse(code = 200, message = "Order atualizado com sucesso.", response = Order.class),
			@ApiResponse(code = 400, message = "Mandatory parameter not found; Validation of unfulfilled rules.", response = MensagemRetorno.class)})
	public ResponseEntity<Order> patch(Map<String, String> update, @ApiParam(value="Order ID", required= true) @Min(1) Long id);
	
	@ApiOperation(value = "Delete a Order", 
			notes = "No payload is returned on success. Failing returns 'MensagemRetorno'")
	@ApiResponses(value = {
			@ApiResponse(code = 400, message = "Mandatory parameter not found; Validation of unfulfilled rules.", response = MensagemRetorno.class)})
	public void delete(@ApiParam(value="Order ID", required= true) @Min(1) Long id);
}