package br.com.brasilprev.lojavirtual.rest.controller;

import java.util.Map;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.web.PageableDefault;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.util.StringUtils;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PatchMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import br.com.brasilprev.lojavirtual.domain.exception.NotFoundException;
import br.com.brasilprev.lojavirtual.domain.exception.OperationException;
import br.com.brasilprev.lojavirtual.domain.model.Customer;
import br.com.brasilprev.lojavirtual.domain.service.CustomerService;
import br.com.brasilprev.lojavirtual.rest.controller.docs.CustomerControllerDocs;
import br.com.brasilprev.lojavirtual.rest.payload.response.Pagination;

@RestController
@Validated
@RequestMapping("/api/customers")
public class CustomerController implements CustomerControllerDocs {

    @Autowired
    private CustomerService customerService;
    
    @GetMapping(value={""}, produces = MediaType.APPLICATION_JSON_VALUE)
	@PreAuthorize("hasRole('BASIC')")
	public ResponseEntity<Pagination<Customer>> findAll(Customer customerFilter, @PageableDefault Pageable pageable, HttpServletRequest request) {
		Page<Customer> page = customerService.findAll(customerFilter, pageable);
    	Pagination<Customer> pagination = new Pagination<>(page, request);
		return ResponseEntity.ok(
			pagination
		);
	}
    
    @GetMapping(value={"/{id}"}, produces = MediaType.APPLICATION_JSON_VALUE)
	@PreAuthorize("hasRole('BASIC')")
	public ResponseEntity<Customer> findById(@PathVariable Long id) {
		return ResponseEntity.ok(
			customerService.findById(id).orElseThrow(() -> new NotFoundException("Customer not found"))
		);
	}
    
    @PostMapping(value={"/create"}, produces = MediaType.APPLICATION_JSON_VALUE)
	@PreAuthorize("hasRole('BASIC')")
	public ResponseEntity<Customer> create(@RequestBody Customer newCustomer) {
		Customer customer = customerService.save(newCustomer);
		return ResponseEntity.ok(
			customer
		);
	}
    
    @PutMapping("/{id}")
	@PreAuthorize("hasRole('BASIC')")
    public ResponseEntity<Customer> saveOrUpdate(@RequestBody Customer customer, @PathVariable Long id) {

        return ResponseEntity.ok(
    		customerService.findById(id)
            .map(x -> {
                x.setName(customer.getName());
                return customerService.save(x);
            })
            .orElseGet(() -> {
            	customer.setId(id);
                return customerService.save(customer);
            })
		);
    }
    
    @PatchMapping("/{id}")
	@PreAuthorize("hasRole('BASIC')")
    public ResponseEntity<Customer> patch(@RequestBody Map<String, String> update, @PathVariable Long id) {

        return ResponseEntity.ok(
    		customerService.findById(id)
            .map(x -> {

                String name = update.get("name");
                if (!StringUtils.isEmpty(name)) {
                    x.setName(name);

                    // better create a custom method to update a value = :newValue where id = :id
                    return customerService.save(x);
                } else {
                    throw new OperationException("Atualização do(s) campo(s) " + update.keySet().toString() + " não é permitida.");
                }

            })
            .orElseGet(() -> {
                throw new NotFoundException("Customer não encontrado");
            })
		);
    }

    @DeleteMapping("/{id}")
	@PreAuthorize("hasRole('BASIC')")
    public void delete(@PathVariable Long id) {
    	customerService.deleteById(id);
    }

}
